\providecommand{\pmode}{beamer} % fallback to beamer presentation
\documentclass[\pmode]{beamer}
\usepackage{gitinfo2}
\usepackage{setspace}
\usepackage[italian]{babel}
\usepackage{tabularx}
\usepackage{xspace}
\usepackage{tabularray}

\newcommand{\handoutpresentation}{handout}
%\ifx\pmode\handoutpresentation
\mode<handout>{%
	\usepackage{handoutWithNotes}
	\pgfpagesuselayout{2 on 1 with notes}[a4paper,border shrink=1mm,landscape]
}
%\fi
\mode<handout>{\setbeamercolor{background canvas}{bg=black!2}}

\newcommand{\rootdir}{../..}
\newcommand{\imagedir}{\rootdir/images}
\newcommand{\exampledir}{\rootdir/examples}
\newcommand{\lilydir}{\rootdir/images/ly-out}

\input{\rootdir/macros}

% added gitinfo2 marks

\title{Fausto Romitelli's\\\NMCF (1991)\\A short presentation\\[-0.25\baselineskip] {\tiny (\versiontag)}}
\author{Nicola Bernardini}
\date{24 maggio 2024}
\institute[Chigiana]{Accademia Chigiana -- Seminario Live--Electronics and Sound \& Music Computing}

\begin{document}

\begin{frame}
  \titlepage
  
  \makebox[\textwidth][l]{%
      \makebox[0.1\textwidth][l]{\includegraphics[width=0.1\textwidth]{\imagedir/cc}}
      \parbox[l]{0.9\textwidth}{
      \begin{spacing}{0.5}
	 {\tiny These slides are released under the Creative Commons
	 Attribution -- ShareAlike 4.0 License. To read the licence text in
	 full please visit \url{http://creativecommons.org/licenses/by-sa/4.0/en/} or
         send a mail to Creative Commons, 171 Second Street, Suite 300, San
         Francisco, California, 94105, USA.}
       \end{spacing}
     }
  }
\end{frame}

\section{Context}

\newcounter{ms}
\setcounter{ms}{0}

\refstepcounter{ms}
\begin{frame}
  \frametitle<+->{\Rom -- (\arabic{ms})}

  \begin{itemize}[<+- | alert@+>]

      \item born in Gorizia in 1963\uncover<+->{, dies in Milano in 2004 at the age of 40}

      \item studies at the Milan Conservatory with Umberto Rotondi,
            then at the Accademia Chigiana and at Scuola Civica di Milano with
	    Franco Donatoni

      \item applies and participates to the \emph{Cursus de Composition et
            Informatique Musicale} at IRCAM in Paris in 1990--1991

      \item while at IRCAM, he comes in contact with the spectralist movement
      and with the connection between music and scientific research\uncover<+->{, concepts
      which will accompany his work for the rest of his life}

      \item upon finishing the \emph{Cursus}, he is invited from 1993 to 1995
            as \emph{compositeur en recherche} at IRCAM where he studies with
            G\'erard Grisey (1946--1998) and Hugues Dufourt.

  \end{itemize}

\end{frame}

\refstepcounter{ms}
\begin{frame}
  \frametitle<+->{\Rom -- (\arabic{ms})}

  \begin{itemize}[<+- | alert@+>]

	\item since very early on in his production, \rom introduces electric
	instruments (such as electric guitars, keyboards, synthesizers, etc.)
	in the instrumentation

        \item on the compositional side, he often mixes rethoric gestures
	lifted out from the rock/pop realm (such as \emph{power chords},
	\emph{distortion}, \emph{dirty harmonies}, etc.)
	with sophisticated harmonic/textural thinking typical of contemporary
	music 

	\item as such, he is considered to be a \emph{non--formalist} who ``does
	not shy away from hybridization of popular and art music''\ldots

	\item \ldots but my take is that he was trying to integrate some
	connotative aspects of the global a--historical roots that we all have
	with the specific craft of occidental contemporary music composition

  \end{itemize}

\end{frame}

\setcounter{ms}{0}

\refstepcounter{ms}
\begin{frame}
  \frametitle<+->{\Rom -- a few quotes (\arabic{ms})}

  \begin{itemize}[<+- | alert@+>]

	\item ``Since I was born, I bathe in the digitized images,
		synthetic sounds, artifacts. The artificial,
		the distorted, filtered -- that's what the Nature of men is today.''

	\item ``I believe that popular music has changed our perception of sound
	      and established new forms of communication [\ldots]
	      The boundless energy, violent and visionary impact, the relentless search
	      for new sounds able to open the "doors of perception": these aspects
	      of most innovative rock seem to join the expression worries some contemporary composers.''

    \end{itemize}

\end{frame}

\refstepcounter{ms}
\begin{frame}
  \frametitle<+->{\Rom -- a few quotes (\arabic{ms})}

  \begin{itemize}[<+- | alert@+>]

	\item ``At the centre of my composing lies the idea of considering sound
	        as a material into which one plunges in order to forge
		its physical and perceptive characteristics:
		grain, thickness, porosity, luminosity, density and elasticity.
		\uncover<+- | alert@+->{%
		Hence it is sculpture of sound, instrumental synthesis, anamorphosis,
		transformation of the spectral morphology, and a constant drift towards
		unsustainable densities, distorsions and interferences, thanks also
		to the assistance of electro-acoustic technologies.
		An increasing importance is given to the sonorities of non-academic
		derivation and to the sullied, violent sound of a prevalently metallic
		origin of certain rock and techno music.''}

  \end{itemize}

\end{frame}

\section{\NMCF}

\setcounter{ms}{0}

\refstepcounter{ms}
\begin{frame}
	\frametitle<+->{\NMCF -- (\arabic{ms})}

	\begin{itemize}[<+- | alert@+>]

		\item The work \NMCF is composed between 1991 and 1992, during
		the \emph{Cursus} at IRCAM

		\item It is a work for amplified string quartet, fixed media
		      and 4--channel spatialization system

		\item it is the first among \rom's works to actually use
		electronics\uncover<+- | alert@+->{ (although he had already been using
		electric instruments and synthesizers)}

		\item it was premiered on March 19, 1992 in the \emph{\'Espace
		de projection} at \ir with the \emph{Quatuor Simon}

	\end{itemize}

\end{frame}

\refstepcounter{ms}
\begin{frame}
	\frametitle<+->{\NMCF -- (\arabic{ms}) -- program notes}

	\begin{itemize}[<+- | alert@+>]

		\item In the original program notes (in French), \rom writes:\\[\baselineskip]

		\begin{quote}
                       Le travail de synth\`ese met le compositeur
		       en face d'une probl\'ematique nouvelle :
		       le son comme objet isol\'e, pr\'e-syntaxique.
		       Pour \'eclairer l'influence de cette notion sur mon approche compositionnelle,
		       je me servirai d'une m\'etaphore tir\'ee de la critique litt\'eraire : celle du NOM.
		\end{quote}

	\end{itemize}

\end{frame}

\refstepcounter{ms}
\begin{frame}
	\frametitle<+->{\NMCF -- (\arabic{ms}) -- program notes}

	\begin{itemize}[<+- | alert@+>]

		\item \begin{quote}
			On peut penser \`a une \'echelle imaginaire :
			\`a une extr\'emit\'e, une langue sans Mots, exclusivement relationnelle,
			faite d'\'el\'ements minimaux, conventionnels et abstraits,
			qui se r\'ealise par proc\'ed\'es alg\'ebriques et combinatoires [\ldots]\\
			\end{quote}

		\item \begin{quote} % a Cratylus language
			[\ldots] \`a l'autre extr\'emit\'e de l'\'echelle,
			         une langue de Mots ou plut\^ot de Noms,
				 dans laquelle les r\'eseaux de relations
				 sont seulement virtuels, comme \'emanations des Noms eux-m\^emes.
		\end{quote}

		\item \begin{quote}
			Si le son de synth\`ese est une image sonore fusionn\'ee, singuli\`ere et complexe,
			articul\'ee en son int\'erieur, qui se donne compl\`etement \`a l'intuition
			et reste opaque \`a l'analyse, alors la langue de la synth\`ese est une langue de Noms.
			\end{quote}

	\end{itemize}

\end{frame}

\refstepcounter{ms}
\begin{frame}
	\frametitle<+->{\NMCF -- (\arabic{ms}) -- program notes}

	\begin{itemize}[<+- | alert@+>]

		\item Several quotes from \emph{Le degr\'e z\'ero de l'\'ecriture} by Roland Barthes and from
		      the \emph{Remarks on color} by Ludwig Wittgenstein % written in 1950, one year before his death

		\item The last quote from Wittgenstein is interesting:\\[\baselineskip]

			\begin{quote}
			145. Nor can we say that white is essentially the property of a visual surface.
			For it is conceivable that white should occur as a highlight or as the colour of a flame.
			\end{quote}

	\end{itemize}

\end{frame}

\setcounter{ms}{0}
\refstepcounter{ms}
\begin{frame}
	\frametitle<+->{\NMCF -- general form}

	\begin{itemize}[<+- | alert@+>]

		\item although it is conceived in a single movement,
		      the work is divided very clearly in 7 parts:\\[0.5\baselineskip]

		\begin{itemize}[<+- | alert@+>]

			\item {\bfseries A} -- a slow, harmonic and choral--like introduction in slowly ascending pattern
			                       with ample electronic \emph{pads}

			\item {\bfseries B} -- more \emph{agitato}, where all
					the figures of the quartet are introduced in a
					\emph{stretto} counterpoint

			\item {\bfseries C} -- very quick tempo, with large harmonies constantly modified timbrically
			                       (somewhat a combination of {\bfseries A} and {\bfseries B})

		\end{itemize}

		\item \ldots

	\end{itemize}

\end{frame}

\refstepcounter{ms}
\begin{frame}
	\frametitle<+->{\NMCF -- general form}

	\begin{itemize}[<+- | alert@+>]

		\item \ldots

		\begin{itemize}[<+- | alert@+>]

			\item {\bfseries C.1} (or {\bfseries C.bis}) -- keeping
						the quick tempo, the timbral modifications dissolve
						and give way to a raising pattern

			\item {\bfseries D} -- always quick tempo,
			combinations of glissandi (with and without \emph{j\'et\'e}s) and super--vibratos,
			introduction of ultrafast--arpeggios 

			\item {\bfseries E} and {\bfseries F} -- all the
			gestures are now present and start to rotate from
			instrument to instrument; in {\bfseries F} the fixed
			media \emph{tacet} and the quartet concludes the piece
			alone. In the last part, a very timid consonant
			cellular motive based on a repeated minor third (D--B)
			appears.
			\raisebox{-8ex}{\pgfimage[width=0.4\textwidth]{\imagedir/NMCF-F-melodic_motive.png}}
		
		\end{itemize}

	\end{itemize}

\end{frame}

\setcounter{ms}{0}
\refstepcounter{ms}
\begin{frame}
	\frametitle<+->{\NMCF -- compositional patterns}

	\begin{center}
		\begin{figure}
			\hspace*{-0.5cm}\pgfimage[width=1.1\textwidth]<1 | handout:0>{\imagedir/NMCF-bars-118-120-0.png}
			                \pgfimage[width=1.1\textwidth]<2 | handout:0>{\imagedir/NMCF-bars-118-120-1.png}
			                \pgfimage[width=1.1\textwidth]<3 | handout:0>{\imagedir/NMCF-bars-118-120-2.png}
			                \pgfimage[width=1.1\textwidth]<4 | handout:1>{\imagedir/NMCF-bars-118-120-3.png}
			\caption{\ref{fig:NMCF_118_120} \NMCF bars 118--120\label{fig:NMCF_118_120}}
		\end{figure}
	\end{center}

\end{frame}

\setcounter{ms}{0}
\refstepcounter{ms}
\begin{frame}
	\frametitle<+->{\NMCF -- tempo structure}

	\begin{center}
	\begin{tblr}{colspec={r Q[l,0.5em] Q[l,6em]}, rowspec={Q[t]|Q[t]|Q[t]}}
		{\bfseries A}: &
			\raisebox{-1.2ex}{\pgfimage[width=1cm]{\imagedir/NMCF-tempo_1.png}} &
			\raisebox{-1.2ex}{\input{\lilydir/tempo_4_60}} \\
		{\bfseries B}: &
			\raisebox{-1.2ex}{\pgfimage[width=1cm]{\imagedir/NMCF-tempo_2.png}} &
			\raisebox{-1.2ex}{\input{\lilydir/tempo_4_120}} \\
		{\bfseries C}, {\bfseries C.1}, {\bfseries D}, {\bfseries E} and {\bfseries F}: &
			\raisebox{-1.4ex}{\pgfimage[width=1cm]{\imagedir/NMCF-tempo_3.png}} &
			\raisebox{-1.2ex}{\input{\lilydir/tempo_4_240}} \\
	\end{tblr}
	\end{center}

\end{frame}


\setcounter{ms}{0}
\refstepcounter{ms}
\begin{frame}
	\frametitle<+->{\NMCF -- score indications (\arabic{ms})}

	\begin{center}
		\begin{figure}
			\pgfimage[height=0.75\textheight]{\imagedir/NMCF-sistema_di_diffusione.png}
			\caption{\ref{fig:NMCF_diffusion} \NMCF diffusion system\label{fig:NMCF_diffusion}}
		\end{figure}
	\end{center}

\end{frame}

\refstepcounter{ms}
\begin{frame}
	\frametitle<+->{\NMCF -- score indications (\arabic{ms})}

	\begin{center}
		\begin{figure}
	\hspace*{-0.5cm}\pgfimage[width=1.1\textwidth]{\imagedir/NMCF-quartet_spatialization.png}
			\caption{\ref{fig:quartet_spatialization} \NMCF quartet spatialization\label{fig:quartet_spatialization}}
		\end{figure}
	\end{center}

\end{frame}

\refstepcounter{ms}
\begin{frame}
	\frametitle<+->{\NMCF -- score indications (\arabic{ms})}

	\begin{center}
		\begin{figure}
	\hspace*{-0.5cm}\pgfimage[width=1.1\textwidth]{\imagedir/NMCF-sequences.png}
			\caption{\ref{fig:sequences} \NMCF sequencing description\label{fig:sequences}}
		\end{figure}
	\end{center}

\end{frame}

\refstepcounter{ms}
\begin{frame}
	\frametitle<+->{\NMCF -- score indications (\arabic{ms})}

	\begin{center}
		\begin{figure}
	\hspace*{-0.5cm}\pgfimage[width=1.1\textwidth]{\imagedir/NMCF-synthetic_sounds.png}
			\caption{\ref{fig:synthetic_sounds} \NMCF synthetic sound description\label{fig:synthetic_sounds}}
		\end{figure}
	\end{center}

\end{frame}

\refstepcounter{ms}
\begin{frame}
	\frametitle<+->{\NMCF -- the fixed--media part}

	\begin{itemize}[<+- | alert@+>]

		\item six sonic fragments, corresponding to the first 6 parts:

		\begin{itemize}[<+- | alert@+>]

			\item {\bfseries A}: \hspace*{0.95em}\listento{\exampledir/NMCF_A-fragment.ogg}{long wide low \emph{pad} textures}

			\item {\bfseries B}: \hspace*{0.95em}\listento{\exampledir/NMCF_B-fragment.ogg}{low \emph{pad} textures with strongly resonant filters}

			\item {\bfseries C}: \hspace*{0.95em}\listento{\exampledir/NMCF_C-fragment.ogg}{short mixture bursts}

			\item {\bfseries C.1}: \listento{\exampledir/NMCF_Cbis-fragment.ogg}{elongated filtered mixtures bursts}

			\item {\bfseries D}: \hspace*{0.9em}\listento{\exampledir/NMCF_D-fragment.ogg}{low drones with short acute bursts}

			\item {\bfseries E}: \hspace*{0.99em}\listento{\exampledir/NMCF_E-fragment.ogg}{long vocal--like \emph{pad} textures}

		\end{itemize}

	\end{itemize}

\end{frame}

\refstepcounter{ms}
\begin{frame}
	\frametitle<+->{\Rom -- \NMCF}

	\begin{center}
		\listento{\exampledir/Romitelli-NMCF-Paul_Klee_4tet_Zavagna.ogg}{Paul Klee Quartet -- Paolo Zavagna, elettronica}
	\end{center}

\end{frame}

\refstepcounter{ms}
\begin{frame}
	\frametitle<+->{Sources for this presentation}

	\begin{center}
		\url{https://gitlab.com/nicb/romitellis-NMCF-a-short-presentation}
	\end{center}

\end{frame}

\end{document}
